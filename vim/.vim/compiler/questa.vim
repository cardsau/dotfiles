" Vim compiler file
" Compiler: Compiler Suite from Mentor Graphics Questa
" Maintainer:
" Latest Revision:

if exists("current_compiler")
  finish
endif
let current_compiler = "questa"

if exists(":CompilerSet") != 2
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo&vim

CompilerSet makeprg=make

" clear
CompilerSet errorformat=

" set directory stack tracing.
CompilerSet errorformat+=%Dmake:\ Entering\ directory\ %[`']%f'
CompilerSet errorformat+=%Xmake:\ Leaving\ directory\ %[`']%f'

" call make in make
CompilerSet errorformat+=%Dmake[%\\d%\\+]:\ Entering\ directory\ %[`']%f'
CompilerSet errorformat+=%Xmake[%\\d%\\+]:\ Leaving\ directory\ %[`']%f'

"
" if a transcript file should be parsed each lines starts with string "# "
" followed by the output of vcom/vlog/vlib ...
" Therefore these two characters are optional for the errorformat
"

" generic Error for an explicit given file
CompilerSet errorformat+=#%\\?\ %\\?**\ %trror:\ %f(%l):\ %m
"
" generic Error without given file (i.e. in case of file not found or lib can't created)
CompilerSet errorformat+=#%\\?\ %\\?**\ %trror:\ (%\\h%\\+-%\\d%\\+)\ %m

" generic Warning for an explicit given file
CompilerSet errorformat+=#%\\?\ %\\?**\ %tarning:\ %f(%l):\ %m
"
" generic Error without a given file
CompilerSet errorformat+=#%\\?\ %\\?**\ %tarning:\ (%\\h%#-%\\d%#)\ %m

let &cpo = s:cpo_save
unlet s:cpo_save
